package utils

import (
    "list"
    "strings"
  t  "text/template"
)



// FlattenJson takes a Json dictionary and converts it to a "key=value" properties
// file
//
// The received "#params" parameter format must be a JSON with depth=1
//
// Example of use:
//
// $cat test.cue
//   package test
//   import "kumori.systems/utils"
//   let jsonContent = {
//     secA: {
//       key1: "11"
//     }
//     secB: {
//       key2: 22
//       key3: "33"
//     }
//   }
//   flattenContent: { utils.#FlattenJson & {#params: jsonContent} }.#value
//
// $ cue export -e flattenContent .
//   "secA.key1=11\nsecB.key2=22\nsecB.key3=33"
//
#FlattenJson: {
	#p: {...}
	#value: strings.Join(list.SortStrings(list.FlattenN(
		[
			for k, v in #p {
			[
				for i, w in {#FlattenR & {#p: v}}.#value {
				"\(k)\(w)" }
			] }
		], -1)), "\n")
}


// Recurse
//   Factory that generates a limited-depth recursive function, acceptable to CUE
//   starting with a Function factory (returning the function to make pseudo-recursive)
//   and a maximum depth
#RecurseN: {
  // Maximum depth of recursion
  #N: uint

  // The function factory must take in a _next argument, which it
  // should use to make the generated function perform the recursive step.
  // The actual function is returned within the func field,
  // and can implement any suitable in/out protocol
  //
  #funcFactory: {#next: _, func: _}

  let depth = #N
  
  // THe approach is to create a dictionary of functions that call themselves
  // starting from an index, and proceeding down the set of indices in alphabetic order.
  //
  for k, it in depth*["iter"] {
    #Funcs: "\(it)\(k)": (#funcFactory & {#next: #Funcs["\(it)\(k+1)"]}).func
  }

  // The last function is null
  // Will raise an error if reached
  #Funcs: "iter\(depth)": null

  // Export the function at position 0
  Func: #Funcs.iter0
}


#FlattenR: (#RecurseN & {#N: 10, #funcFactory: _#FlattenF}).Func

_#FlattenF: {
  #next: _ 

  func: {
    #p: _
    #value: [...string]
    if (#p & {...}) == _|_ {
      #value: ["=\(#p)"]
    } 
    if (#p & {...}) != _|_ {
      #value: list.FlattenN([
        for k, v in #p {
        [
          for i, w in {#next & {#p: v}}.#value {
          ".\(k)\(w)" }
        ] }
      ], -1)
    }
  }
}

// FlattenCUE
//  Converts an arbitrarily nested struct into a flat struct
//  where fields are formed concatenating the field names
//  in the original struct.
#FlattenCUE: {
	#p: {...}
	#value: _
  let pre = list.FlattenN(
		[
			for k, v in #p {
			[
				for i, w in {#FlattenCUER & {#p: v}}.#value {
				{head: "\(k)\(w.head)", tail: w.tail} }
			] }
		], -1)
  for i, v in pre {
    #value: "\(v.head)": v.tail
  }
}

// The recursive funcion for FlattenCUE
#FlattenCUER: (#RecurseN & {#N: 10, #funcFactory: _#FlattenCUE}).Func

// The generator base function
_#FlattenCUE: {
  #next: _ 

  func: {
    #p: _
    #value: _
    if (#p & {...}) == _|_ {
      _#value: head: ""
      if ((#p & [...string]) != _|_) {
        _#value: tail: strings.Join(#p, ",")
      }
      if (#p & [...string]) == _|_ { // Assume atomic
        _#value: tail: #p
      }
      #value: [_#value]
    } 
    if (#p & {...}) != _|_ {
      #value: list.FlattenN([
        for k, v in #p {
          let recurse = {#next & {#p: v}}.#value
        [
          for i, w in recurse {
          {head: ".\(k)\(w.head)", tail: w.tail} }
        ] }
      ], -1)
    }
  }
}



// FlattenTAGS
//  Converts an arbitrarily nested struct with a final simple struct 
//  into a set of structs keyed by the labels of the leaf struct
//  whose value is either 
//  f: a flattened key-value pair 
//  a: an array of flattened keys
//  t: an array of types from the array field
//  T: a type with optional arrays for each key of t
// 
//  It is assumed that two different simple structs, although with different labels
//  cannot have the same leading path
//
//  FlattentTAGS expects a parameter #k with the valid strings to accept
//
#FlattenTAGS: {
	#p: {...}
  #k: _ 

  let ff = {
    #next: _ 

    func: {
      #p: _
      
      let typ = close({[#k]: _ })
      #value: [...]
      if (#p & typ) != _|_ {
        #value: [{head: "", tail: #p}]
      } 
      if (#p & typ) == _|_ {
        #value: list.FlattenN([
          for k, v in #p {
            let recurse = {#next & {#p: v}}.#value
          [
            for i, w in recurse {
            {head: ".\(k)\(w.head)", tail: w.tail} }
          ] }
        ], -1)
      }
    }
  } 

  let ffr = (#RecurseN & {#N: 10, #funcFactory: ff}).Func

	#value: {
    f: _ 
    a: _
    t: _ 
    T: _
  }

  let pre = {
    for k, v in #p {
      for i, w in {ffr & {#p: v}}.#value {
        for l, z in w.tail {
          "\(l)": "\(k)\(w.head)": z 
        }
      }
    }
  }

  #value: f: pre

  for k, v in pre {
    #value: {
      a: "\(k)": [for i, j in v {i}]
      T: ["\(k)"]: t[k]
      t: "\(k)": or(a[k])
    }
  }
}


// JsonToIni takes a Json dictionary whose keys are section names, and converts
// it to an INI format file, where each section name is enclosed within [] in
// its own line and the key/value pairs are written within their own line
// afterwards
//
// The received "#params" parameter format must be: section:key:value
//
// Example of use:
//
// $cat test.cue
//   package test
//   import "kumori.systems/utils"
//   let jsonContent = {
//     secA: {
//       key1: "11"
//     }
//     secB: {
//       key2: 22
//       key3: "33"
//     }
//   }
//   iniContent: { utils.#JsonToIni & {#params: jsonContent} }.#value
//
// $ cue export -e iniContent .
//   "[secA]\nkey1=11\n\n[secB]\nkey2=22\nkey3=33\n\n"
//
#JsonToIni: {
  $p = #params: [string]: [string]: _
	// Be careful with multiline templates: dont use "tabs" at the beginning of
	// the lines (multiline are not mandatory: using "{{println}}" all the template
	// can be one line)
	let template = ##"""
    {{range $k,$v := .}}{{printf "[%s]" $k}}
    {{range $sk,$sv := .}}{{printf "%s=%v" $sk $sv}}
    {{end}}
    {{end}}
    """##
  #value: t.Execute(template,$p)
}

// Given a dot-separated path in a string, retrieve the
// value from the structure provided
#Retrieve: {
  $p=#p: {...}
  #k: string
  let keys = strings.Split(#k, ".")

  #value: {#RetrieveR & {#p: $p, #k: keys}}.#value
}


#RetrieveR: (#RecurseN & {#N: 100, #funcFactory: _#RetrieveF}).Func

_#RetrieveF: {
  #next: _ 

  func: {
    #p: _
    #k: [...string]
    #value: _

    if len(#k) == 0 {
      #value: #p
    } 
    if  len(#k) > 0 {
      let r = list.Slice(#k,1,len(#k))
      let v = #p[#k[0]]

      #value: {#next & {#p: v, #k: r}}.#value 
    }
  }
}


// Utility function for projecting all fields except those
// provided in "fields"
// Additionally, it adds the fields in opt if they are absent from 
// ins
#NProjectF : {
	$p=#p: {
		ins: {...}
    defaults: {...}
		fields: [...string]
	}
	let ft  = or($p.fields)

	out: {
		for k,v in $p.ins if (k & ft) == _|_ {
			"\(k)": v
		}
    for k, v in $p.defaults if ((k & ft) == _|_) && ($p.ins[k] == _|_) {
      "\(k)": v
    }
	}
}

// Utility function for projecting all fields provided in "fields"
// Additionally, it adds the fields in opt if they are absent from 
// ins
#ProjectF : {
	$p=#p: {
		ins: {...}
    defaults: {...}
		fields: [...string]
	}

	out: {
		for i,k in $p.fields {
      if $p.ins[k] != _|_ {
        "\(k)": $p.ins[k]
      }
      if ($p.ins[k] == _|_) && ($p.defaults[k] != _|_) {
        "\(k)": $p.defaults[k]
      }
		}
	}
}